//
//  HomeFinderViewController.swift
//  The Villages
//
//  Created by Joshua Hale on 5/8/18.
//  Copyright © 2018 Joshua Hale. All rights reserved.
//

import UIKit

class HomeFinderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(navigationBar: self.navigationController?.navigationBar)

        APIManager.sharedInstance.getHomes(onSuccess: {
            json in DispatchQueue.main.async {
                //-- TODO: Add to list
            }
        }, onFailure: { error in
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            self.show(alert, sender: nil)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
