//
//  UIViewController+Extensions.swift
//  The Villages
//
//  Created by Joshua Hale on 5/8/18.
//  Copyright © 2018 Joshua Hale. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setupNavigationBar(navigationBar: UINavigationBar?) {
        navigationBar?.barTintColor = UIColor(named:"Villages Green")
    }
    
}
