//
//  APIManager.swift
//  The Villages
//
//  Created by Joshua Hale on 5/8/18.
//  Copyright © 2018 Joshua Hale. All rights reserved.
//

import Foundation
import SwiftyJSON


class APIManager {
    
    let baseUrl = "https://www.thevillages.com/"
    
    static let sharedInstance = APIManager()
    static let getHomesEndpoint = "vps/JsonData/VillagesHomefinderMobiApp.json"
    static let homefinderFiltersEndpoint = "vps/JsonData/hf_filters.json"
    static let homeDetailsEndpoint = "Homefinder/Search/GeneratedDetailView"
    
    func getHomefinderFilters(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        let request = buildGetRequest(endpoint: APIManager.homefinderFiltersEndpoint)
        
        let task = getSessionTask(request: request, onSuccess: onSuccess, onFailure: onFailure)
        task.resume()
    }
    
    func getHomes(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        let request = buildGetRequest(endpoint: APIManager.getHomesEndpoint)
        let task = getSessionTask(request: request, onSuccess: onSuccess, onFailure: onFailure)
        task.resume()
    }
    
    fileprivate func getSessionTask(request: NSMutableURLRequest, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) -> URLSessionDataTask {
        let session = URLSession.shared
        return session.dataTask(with: request as URLRequest, completionHandler: { data, response, error -> Void in
            if (error != nil) {
                onFailure(error!)
            } else {
                do {
                    let result = try JSON(data: data!)
                    onSuccess(result)
                } catch {
                    onFailure(error)
                }
            }
        })
    }
    
    fileprivate func buildGetRequest(endpoint: String) -> NSMutableURLRequest {
        let url = baseUrl + endpoint
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        return request
    }
}
